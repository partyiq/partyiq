from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^(?P<metro>[^/]+)/$', 'events.views.front_landing', name='home-page'),
                       url(r'^$', 'events.views.front_landing', name='home-page'),
                        url(r'^(?P<metro>[^/]+)/events/(?P<type>[^/]+)$', 'events.views.event_search', name='events-page'),
                       url(r'^(?P<metro>[^/]+)/events/(?P<type>[^/]+)/(?P<date>[^/]+)$', 'events.views.event_search', name='events-page'),
                       url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
                       url(r'^accounts/', include('allauth.urls')),
)
