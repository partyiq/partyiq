#!/usr/bin/env python

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'partyrumbler.settings'
from events.models import Location, Metro
from django.contrib.gis.geos import fromstr

location = Location()
location.center = fromstr("POINT(-117.39616 33.95335)")
location.metro = Metro.objects.get(slug="los_angeles")
location.save()
