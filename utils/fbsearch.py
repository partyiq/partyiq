#!/usr/bin/env python

import sys
import os
from facepy import GraphAPI
from facepy import exceptions
#from facepy.utils import get_application_access_token, get_extended_access_token
os.environ.setdefault('DJANGO_SETTINGS_MODULE','partyrumbler.settings')
from events.models import Place, PlaceStatus
#from django.contrib.gis.geos import fromstr
import pprint
import time

app_id = '200345223495628'
app_secret = '00a7feaf1db74ea5610e38620ec4e3f7'

#page = Place.objects.get(fb_id='140092046195807')
#pages = Place.objects.filter(status='A')
# use Place.objects.values() to get list of values instead of objects to optimize
pages = Place.objects.all()
#pages = [page,]
graph = GraphAPI('CAAC2NoHgZC8wBAF37GXbNrvR7yTNmsXjqWNpZAEfDWojzYjlJqZAZAkB9FoZC5ozrgpT9M3c6GaBZBzcBrzyrwDMyJFqS33qKmPMoZBl5yiU8bz1kUA63yCHDSWUrk4Q064QTXd8qHlpCknqDUUfk1I7xM5SBVjc04fiYAVGWlZCxBkyJFstPc41')
counter=0

# TODO When searching for events in dupes, perhaps we should do CONTAINS for all dupes?
# TODO promoter pages are tricky, they might throw party a party as location that might not be a night club, but an
# event venue, what to do with those.  Search where they throw party, if location is not in DB just ignore, also ignore
# if location is at promoter's location?  See 19402074987
start = time.time()
while True:
    # To avoid populating the queryset cache, but to still iterate over all your results
    # for page in pages.iterator():
    # each iteration execute another query, rather than
    for page in pages:
        try:
            time.sleep(3)
            fb_search_criteria = page.get_fb_search_criteria()
            print "%s:%s" % (fb_search_criteria,page.fb_id)
            json = graph.fql('SELECT name, venue, eid, description, start_time, creator from event \
                              WHERE (eid IN (SELECT eid FROM event_member WHERE uid IN \
                              (SELECT page_id FROM place WHERE page_id = %s)) \
                              AND venue.id = %s) OR (CONTAINS("%s") \
                              and venue.id = %s and creator != %s)'
                             % (page.fb_id, page.fb_id, fb_search_criteria, page.fb_id, page.fb_id))
            counter += 1
            #print json
            if counter % 100 == 0:
                end = time.time()
                elapsed = (start - end) / 60
                print 'COUNTER>>>>>>>>>>>>>>>%s in %s minutes' % (counter, elapsed)
            for event in json['data']:
                print event['eid']
        except exceptions.OAuthError as e:
            print "PageID:%s" % page.fb_id
            print "Counter:%s" % counter
            template = "An exception of type {0} occured. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            print message
            sys.exit(1)
        except Exception as e:
            print "PageID:%s" % page.fb_id
            print "Counter:%s" % counter
            #import traceback
            #print traceback.format_exc()
            #import pdb
            #pdb.post_mortem()
            #An exception of type OAuthError occured. Arguments:
            #(u'[17] (#17) User request limit reached',)
            #if json is not None:
            #    pprint.pprint(json)
            template = "An exception of type {0} occured. Arguments:\n{1!r}"
            message = template.format(type(e).__name__, e.args)
            print message
            pass
