#!/usr/bin/env python

from events.facebook import FBEventSearch
import argparse

parser = argparse.ArgumentParser(description='Facebook events scrapper')
parser.add_argument('-r', action='store_true', default=False,
                    help='Update existing events')
parser.add_argument('-l', action='store', dest='location', type=str,
                    help="Specific city")
parser.add_argument('-s', action='store_true', default=False,
                    help="Single run mode")
parser.add_argument('-q', action='store_true', default=False,
                    help="Process places in quarantine")
parser.add_argument('-b', action='store_true', default=False,
                    help="Process places that have been blacklisted")
results = parser.parse_args()

fbsearch = FBEventSearch(
    location = results.location,
    single_run = results.s,
    quarantine = results.q,
    blacklist = results.b,
    update = results.r,
    graph_speed = 2.9,
    token = 'CAAC2NoHgZC8wBAPicnfsGkDQoSBOLrKaMTzyPeAy8rjGa0a9zyJmIuQfgEkrfZCZBDym4j5scNaESawyLGAj50E8r6sZCZAaEZCAtaHO59LDpsVlm4i4IflGTWVauIvJoGO6clI66hiobuRohtDNZAX31Q1EFA6nmSIVhZB5wuDLHoM41g6c5dR21tALLRZAZB0d2vBGzlr2aGe88RZChOKBKWl2ZBZCGwkrzwmUZD'
)

fbsearch.search_events()
