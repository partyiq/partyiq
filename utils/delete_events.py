#!/usr/bin/env python

import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE','partyrumbler.settings')
from events.models import Location, Metro, Event


Event.objects.filter(place__location__metro__slug='los_angeles').delete()
