#!/usr/bin/env python

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'partyrumbler.settings'
from django.contrib.gis.db.models import Avg, Count, Min, Sum
from events.models import RScore, Place, Event, PlaceStatus, Keyword, Location
from django.db.models import Q
import sys
from events.utils import convert_unicode_to_string
import argparse

parser = argparse.ArgumentParser(description='Place analyzing tool')
parser.add_argument('-l', action='store', dest='location', type=str,
                    help="Specific city")
results = parser.parse_args()

places = Place.objects.filter(Q(location__metro__slug=results.location)).annotate(Count('event')).order_by('-event__count')
for x in places:
    total = Event.objects.filter(Q(place=x)).count()
    maverage = RScore.objects.filter(event__place=x, category__name = 'music').aggregate(Avg('score'))
    caverage = RScore.objects.filter(event__place=x, category__name = 'club').aggregate(Avg('score'))
    events = Event.objects.filter(Q(category__name='club'),Q(place=x))
    both = events.filter(Q(category__name='music')).count()
    
    events = Event.objects.filter(Q(category__name='club'),Q(place=x))
    club = events.filter(~Q(category__name='music')).count()
    
    events = Event.objects.filter(Q(category__name='music'),Q(place=x))
    music = events.filter(~Q(category__name='club')).count()
    
    events = Event.objects.filter(~Q(category__name='music'),Q(place=x))
    disq = events.filter(~Q(category__name='club')).count()
    
    print "========%s=======" % convert_unicode_to_string(x.name)
    print "ID: %s" % x.fb_id
    print "Music only: %s" % music
    print "Club only: %s" % club
    print "Both: %s" % both
    print "Disq: %s" % disq
    print "Total: %s   Music RScore:%s  Club RScore:%s" % (total, maverage['score__avg'], caverage['score__avg'])
    print 
