#!/usr/bin/env python

import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'partyrumbler.settings'
from django.contrib.gis.db.models import Avg, Count, Min, Sum
from events.models import RScore, Place, Event, PlaceStatus, Keyword, Location
from django.db.models import Q
import sys

places = Place.objects.filter(Q(event__category__name='club'),Q(location__metro__slug='san_francisco')).annotate(Count('event')).order_by('-event__count').distinct()[:8]
for x in places:
    print "%s %s" % (x.name, x.event__count)
sys.exit()

#print Place.objects.filter(location__metro__slug='san_francisco').count()
#locations = Location.objects.all()
#for location in locations:
#    print "Processing %s" % location.metro.name
#
#sys.exit()
'''
maverage = RScore.objects.filter(event__place__name='DNA Lounge', category__name = 'music').aggregate(Avg('score'))
print maverage
sys.exit()

k = Keyword.objects.all()
for keyword in k:
    if "'" in keyword.word:
        print ":%s:" % keyword.word.replace("'",'')
    else:
        print ":%s:" %  keyword.word

sys.exit()

page = Place.objects.get(name='DNA Lounge')
l = [ item.name  for item in page.category.all() ]
if set(l).issuperset(set(['Dance Club','Night Club','Performance Venue','Concert Venue'])):
    print "HELLO"
sys.exit()

score = RScore.objects.filter(event__place=page, category__name='music').aggregate(Avg('score'))
print score

sys.exit()
'''
#places = Place.objects.filter(Q(event__category__name='club').distinct()
#for x in places:
#    print x.name

#places = Place.objects.filter((Q(category__name='Night Club')|Q(category__name='Dance Club')|Q(category__name='Lounge')) & (Q(name='Concert Venue')|Q(name='Performance Venue')|Q(name='Jazz Club')))
#places = Place.objects.filter(Q(category__name='Night Club')|Q(category__name='Dance Club')|Q(category__name='Lounge'))
places = Place.objects.filter(Q(event__category__name='music'),Q(location__metro__slug='san_francisco')).annotate(Count('event')).order_by('-event__count')
for x in places:
    print "%s %s" % (x.name, x.event__count)
    #if x.category.filter(Q(name='Concert Venue')|Q(name='Performance Venue')|Q(name='Jazz Club')).exists():
    #    print "%s %s" % (x.name, x.category.all())



sys.exit()

events = Event.objects.filter(Q(rscore__category__name='music'),Q(rscore__score=2),~Q(place__category__name='Concert Venue'),~Q(place__category__name='Performance Venue'),~Q(place__category__name='Jazz Club'))

for x in events:
    print x.name
