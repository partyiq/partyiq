__author__ = 'isarkiso'

from django import template
from django.utils.dateparse import parse_datetime
import re
from pytz import timezone
register = template.Library()

def convert_time(timestamp, local_tz):
    try:
        #m = re.search('(\d{4}-\d{2}-\d{2})T', timestamp)
        #if m:
        #    t = parse_datetime(timestamp)
        #else:
        #    return None
        t = timestamp.astimezone(local_tz).time()
    except Exception:
        return None
    return t

register.filter(convert_time)