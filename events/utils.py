import unicodedata

def convert_unicode_to_string(string):
    return unicodedata.normalize('NFKD', string).encode('ascii','ignore')
