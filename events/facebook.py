'''
    Facebook search related classes
'''

import sys
import os
import resource
from facepy import GraphAPI
from facepy import exceptions
from datetime import date, datetime
os.environ.setdefault('DJANGO_SETTINGS_MODULE','partyrumbler.settings')
from events.models import Place, PlaceStatus, Location, Event, PlaceCategory, EventCategory, RScore, Keyword, KeywordSection
import pprint
from django.contrib.gis.geos import fromstr
from events.utils import convert_unicode_to_string
import time
from django.utils.dateparse import parse_datetime, parse_date
import re
import logging
import logging.handlers
from functools import wraps
from multiprocessing import Process, Queue
import traceback
from django.contrib.gis.db.models import Avg
from django.db.models import Q
from dateutil import relativedelta
from django.utils import timezone
import datetime

def processify(func):
    '''Decorator to run a function as a process.

    Be sure that every argument and the return value
    is *pickable*.

    The created process is joined, so the code does not
    run in parallel.

    '''

    def process_func(q, *args, **kwargs):
        try:
            ret = func(*args, **kwargs)
        except Exception:
            ex_type, ex_value, tb = sys.exc_info()
            error = ex_type, ex_value, ''.join(traceback.format_tb(tb))
            ret = None
        else:
            error = None

        q.put((ret, error))

    # register original function with different name
    # in sys.modules so it is pickable
    process_func.__name__ = func.__name__ + 'processify_func'
    setattr(sys.modules[__name__], process_func.__name__, process_func)

    @wraps(func)
    def wrapper(*args, **kwargs):
        q = Queue()
        p = Process(target=process_func, args=[q] + list(args), kwargs=kwargs)
        p.start()
        p.join()
        ret, error = q.get()

        if error:
            ex_type, ex_value, tb_str = error
            message = '%s (in subprocess)\n%s' % (ex_value, tb_str)
            raise ex_type(message)

        return ret
    return wrapper


class LevelFilter(logging.Filter):
    def __init__(self, level):
        self.level = level
    def filter(self, record):
        return record.levelno == self.level

class FBSearch(object):
    '''
        Facebook Search base class
    '''
    app_id = '200345223495628'
    app_secret = '00a7feaf1db74ea5610e38620ec4e3f7'

    def __init__(self, **kw):
        # first set defaults which might get overwritten by __dict__.update(kw)
        self.graph_speed = 1
        self.__dict__.update(kw)
        #self.__dict__.update(locals()) # do this if you have local vars you want to initialize
        # initialize graph API
        self.graph = GraphAPI(self.token)

    @classmethod
    def _initialize_info_logger(cls, log, maxBytes=104857600, backup_count=5, logFormat="%(asctime)s: %(message)s"):
        logger = logging.getLogger('fbinfo')
        logger.propagate = False
        LogFileHandler = logging.handlers.RotatingFileHandler(log, maxBytes=maxBytes, backupCount=backup_count)
        LogFileHandler.setFormatter(logging.Formatter(logFormat))
        logger.addHandler(LogFileHandler)
        logger.setLevel(logging.INFO)
        return logger

    @classmethod
    def _initialize_warning_logger(cls, log, maxBytes=104857600, backup_count=5, logFormat="%(asctime)s: %(message)s"):
        logger = logging.getLogger('fbwarn')
        warnLogFileHandler = logging.handlers.RotatingFileHandler(log, maxBytes=maxBytes, backupCount=backup_count)
        warnLogFileHandler.setFormatter(logging.Formatter(logFormat))
        logger.addHandler(warnLogFileHandler)
        warnLogFileHandler.addFilter(LevelFilter(logging.WARNING))
        return logger

    @classmethod
    def _initialize_error_logger(cls, log, maxBytes=104857600, backup_count=5, logFormat="%(asctime)s: %(message)s"):
        logger = logging.getLogger('fberror')
        errorLogFileHandler = logging.handlers.RotatingFileHandler(log, maxBytes=maxBytes, backupCount=backup_count)
        errorLogFileHandler.setFormatter(logging.Formatter(logFormat))
        logger.addHandler(errorLogFileHandler)
        errorLogFileHandler.addFilter(LevelFilter(logging.ERROR))
        return logger

    def fbgraph(self, cmd, type='fql'):
        if type == 'get':
            json = self.graph.get(cmd)
        elif type == 'fql':
            json = self.graph.fql(cmd)
        elif type == 'batch':
            json = self.graph.batch(cmd)
        time.sleep(self.graph_speed)
        return json


    def pull_place(self, id):
        '''
        Given facebook id return place data via json

        returns data dictionary or None if not found
        '''
        json = self.fbgraph(
                    'select best_page_id, categories, \
                     company_overview, description, description_html, \
                     emails, general_info, general_manager, hours, \
                    is_permanently_closed, location, \
                    name, page_id, page_url,  \
                    phone, pic, pic_big, pic_cover, \
                    pic_large, pic_small, pic_square, press_contact, price_range, \
                    website \
                    from page where page_id = %s' % id)
        if 'data' not in json or not json['data']:
            return None
        return json['data'][0]

    @staticmethod
    def dict_diff(first, second):
        """ Return a dict of keys that differ with another config object.  If a value is
            not found in one fo the configs, it will be represented by KEYNOTFOUND.
            @param first:   Fist dictionary to diff.
            @param second:  Second dicationary to diff.
            @return diff:   Dict of Key => (first.val, second.val)
        """
        diff = {}
        # Check all keys in first dict
        for key in first.keys():
            if (not second.has_key(key)):
                diff[key] = (first[key], '<KEYNOTFOUND>')
            elif (first[key] != second[key]):
                diff[key] = (first[key], second[key])
        # Check all keys in second dict to find missing
        for key in second.keys():
            if (not first.has_key(key)):
                diff[key] = ('<KEYNOTFOUND>', second[key])
        return diff



class FBPlaceSearch(FBSearch):
    '''
        Facebook Place search and analyze class
    '''

    TYPES = {
        'Dance%20Club':'176139629103647',
        'Night%20Club':'191478144212980',
        'Lounge':'135930436481618',
        'Bar':'777777777777777777',
        'Event%20Venue':'77777777777777',
        'Jazz%20Club':'207290352633942',
        'Concert%20Venue':'179943432047564',
        'Performance%20Venue':'77777777777777',
        }
    # Jazz Club 207290352633942
    # Performance Venue 210881938937928
    # Concert Venue 179943432047564
    # Event Venue 211155112228091
    # Bar 110290705711626


    def __init__(self, **kw):
        # initialize logging
        self.elogger = FBSearch._initialize_error_logger(log='/var/log/fbevents/fbplace_error.log')
        self.wlogger = FBSearch._initialize_warning_logger(log='/var/log/fbevents/fbplace_warning.log')
        self.logger = FBSearch._initialize_info_logger(log='/var/log/fbevents/fbplace.log')
        self.logger.info("Initializing fbplace process...")
        pcat = PlaceCategory.objects.all()
        self.cmap = {}
        for category in pcat:
            self.cmap[category.fb_id] = 1
        super(FBPlaceSearch, self).__init__(**kw)

    def _analyze_place_events(self, page):
        '''
        The purpose of this method is to evaluate events for a given place, to see
        whether place is worthy to move up or move down on the place status scale.
        Newly imported pages start in Quarantine and also page.last_status_change
        is set to current date.  Once status changes page.last_status_change is
        updated.  Below are the current rules
        * Page stays in Quarantine for 3 months, if no action is taken it will be delegated
          to Blacklist
        * Page stays in Blacklist indefinitely (will have to revisit this policy)
        * page stays in Active/Active_events unless there are 0 events in the last 6 months,
          if so it will be delegated to Quarantine.
        '''
        caverage = RScore.objects.filter(event__place=page, category__name = 'club').aggregate(Avg('score'))
        maverage = RScore.objects.filter(event__place=page, category__name = 'music').aggregate(Avg('score'))
        if page.status == PlaceStatus.Active or page.status == PlaceStatus.Active_events:
            if (Event.objects.filter(place=page).count() < 3) or (caverage < 2 and maverage < 2):
                page.status = PlaceStatus.Quarantine
                page.last_status_change = timezone.now()
                print "MOVING %s to Quarantine due to low average and number of events" % page.name
                self.logger.info("MOVING %s to Quarantine due to low average and number of events" % page.name)
                page.save()
            elif Event.objects.filter(place=page).count() == 0:
                r = relativedelta.relativedelta(page.last_status_change, timezone.now())
                print "Page timestamp %s %s" % (page.last_status_change, page.name)
                if hasattr(r,'months') and r.months <= -6:
                    page.status = PlaceStatus.Quarantine
                    page.last_status_change = timezone.now()
                    print "MOVING %s to Quarantine due to lack of events" % page.name
                    self.logger.info("MOVING %s to Quarantine due to lack of events" % page.name)
                    page.save()
        elif page.status == PlaceStatus.Quarantine:
            if Event.objects.filter(place=page).count() > 3:
                if caverage >= 2 or maverage >= 2:
                    page.status = PlaceStatus.Active
                    page.last_status_change = timezone.now()
                    page.save()
                    print "MOVING page %s to Active" % page.name
                    self.logger.info("MOVING page %s to Active" % page.name)
                else:
                    r = relativedelta.relativedelta(page.last_status_change, timezone.now())
                    if hasattr(r,'months') and r.months <= -3:
                        page.status = PlaceStatus.Blacklist
                        page.last_status_change = timezone.now()
                        page.save()
                        self.logger.info("MOVING page %s to Blacklist due to low avearage" % page.name)
                        print "MOVING page %s to Blacklist due to low avearage" % page.name
            elif Event.objects.filter(place=page).count() == 0:
                r = relativedelta.relativedelta(page.last_status_change, timezone.now())
                if hasattr(r,'months') and r.months <= -3:
                    page.status = PlaceStatus.Blacklist
                    page.last_status_change = timezone.now()
                    print "MOVING page %s to Blacelist due lack of events"
                    self.logger.info("MOVING page %s to Blacelist due lack of events")
                    page.save()
            else:
                r = relativedelta.relativedelta(page.last_status_change, timezone.now())
                if hasattr(r,'months') and r.months <= -6:
                    page.status = PlaceStatus.Blacklist
                    page.last_status_change = timezone.now()
                    page.save()
                    print "MOVING %s to Blacklist due to low number of events"
                    self.logger.info("MOVING %s to Blacklist due to low number of events")
        elif page.status == PlaceStatus.Blacklist and (Event.objects.filter(place=page).count() > 3 and (caverage >= 2 or maverage >= 2)):
            page.status = PlaceStatus.Active
            page.last_status_change = timezone.now()
            page.save()
            print "MOVING page %s to Active" % page.name
            self.logger.info("MOVING page %s to Active" % page.name)


    def _check_if_within_location(self, data, location):
        try:
            l = Location.objects.get(id=location.id, center__distance_lte=(
                                fromstr("POINT(%s %s)" % (data['location']['longitude'],data['location']['latitude'])),
                                location.distance))
        except Location.DoesNotExist:
            return
        return l

    def _check_if_valid_type(self, data):
        if 'category_list' not in data:
            return False
        for id in data['category_list']['id']:
            if id in FBPlaceSearch.TYPES.values():
                return True
        return False

    def add_place(self, data, location, status=PlaceStatus.Quarantine):
        try:
            if not self._check_if_within_location(data, location):
                print "ATTENTION tried to add page outside of location zone %s %s %s %s" \
                    % (data['page_id'], data['name'], location.center, location.metro.name )
                self.logger.info("ATTENTION tried to add page outside of location zone %s %s %s %s" \
                                      % (data['page_id'], data['name'], location.center, location.metro.name ))
                return
            print "ADDING %s %s" % (data['page_id'], data['name'])
            self.logger.info("ADDING %s %s" % (data['page_id'], data['name']))
            place = Place()
            place.name = data['name']
            place.fb_id = int(data['page_id'])
            place.data = data
            place.point = fromstr("POINT(%s %s)" % (data['location']['longitude'],
                                                    data['location']['latitude']))
            place.status = status
            place.location = location
            place.save()
            if 'categories' in data:
                for item in data['categories']:
                    if item['id'] in self.cmap:
                        place.category.add(PlaceCategory.objects.get(fb_id=item['id']))
                place.save()
        except KeyError as e:
            print "ERROR occured: %s" % e
            self.logger.info("ATTENTION key error occured, place might have incomplete data %s" % e)
            print e
            self.elogger.error(e)

    def analyze_place_location(self):
        '''
        Analyze existing place location to see if there are dupes
        '''
        locations = Location.objects.all()
        for location in locations:
            places = Place.objects.filter(location=location)
            #### ANALYZE ADDRESSES
            proc = []
            for place in places:
                if place in proc:
                    continue
                dupes = Place.objects.filter(point__distance_lte=(place.point, 20)).exclude(fb_id=place.fb_id)
                if dupes:
                    for dupe in dupes:
                        if place.status != 'X' and dupe.status != 'X':
                            self.wlogger.warning("%s:%s:%s has potential dupe %s:%s:%s" \
                                  % (place.name, place.fb_id, place.status,  dupe.name, dupe.fb_id, dupe.status))
                        proc.append(dupe)

    def _analyze_existing_places(self, location):
        ''' Analyzes existing places and makes adjustments'''
        places = Place.objects.filter(location=location)
        for place in places:
            # First check if place still exists
            data = self.pull_place(place.fb_id)
            if not data:
                print "ACTION page no longer exists: %s" % place.fb_id
                self.logger.info("ACTION page no longer exists: %s" % place.fb_id)
                continue
            if not place.data:
                diff = 1
            else:
                diff = FBSearch.dict_diff(place.data, data)

            if diff:
                print "ATTENTION updating page %s %s" % (place.fb_id, place.name)
                self.logger.info("ATTENTION updating page %s %s" % (place.fb_id, place.name))
                place.data = data
                # check categories
                if not place.category_override:
                    for item in data['categories']:
                        if item['id'] in self.cmap:
                            place.category.add(PlaceCategory.objects.get(fb_id=item['id']))
                # next check if place has closed
                if place.status != PlaceStatus.Banned and str(data['is_permanently_closed']) == 'True':
                    print "ATTENTION CLOSED %s %s" % (place.fb_id, place.name) 
                    self.logger.info("ATTENTION CLOSED %s %s" % (place.fb_id, place.name))
                    place.status=PlaceStatus.Banned
       
                # check if best_page_id is not the same and if it's not see where it is
                if data['best_page_id'] and str(data['page_id']) != str(data['best_page_id']) \
                    and not Place.objects.filter(fb_id=int(data['best_page_id'])).exists():
                    bestd = self.pull_place(data['best_page_id'])
                    if bestd:
                    	if not self._check_if_valid_type(bestd):
                            print "ATTENTION tried to add best page not in category list %s %s" % (bestd['page_id'], bestd['name'])
                       	    self.logger.info("ATTENTION tried to add best page not in category list %s %s" % (bestd['page_id'], bestd['name']))
                    	else:
                            self.add_place(bestd, location)

                if convert_unicode_to_string(place.name) != convert_unicode_to_string(data['name']):
                    print "ATTENTION RENAMED FROM %s %s to %s" % (place.fb_id, place.name, data['name'])
                    self.logger.info("ATTENTION RENAMED FROM %s %s to %s" % (place.fb_id, place.name, data['name']))
                    place.name = data['name']
                place.save()
            # analyzing events
            self._analyze_place_events(place)

    def _find_new_places(self, location):
        '''Finds new places'''
        for type_name in FBPlaceSearch.TYPES:
            json = self.fbgraph('search?q=%s&type=place&center=%s,%s&distance=%s'
                                    % (type_name,
                                        location.center.y,
                                        location.center.x,
                                        location.distance), 'get')
            for data in json['data']:
                for category in data['category_list']:
                    if category['id'] in FBPlaceSearch.TYPES.values():
                        if not Place.objects.filter(fb_id=int(data['id'])).exists():
                            json = self.pull_place(data['id'])
                            if str(json['is_permanently_closed']) == 'True':
                                 break
                            if json['best_page_id'] and str(json['page_id']) != str(json['best_page_id']):
                                if not Place.objects.filter(fb_id=int(json['best_page_id'])).exists():
                                    bestd = self.pull_place(json['best_page_id'])
                                    if bestd:
                                        self.add_place(bestd, location)
                            self.add_place(json, location)
                        break

    def analyze(self):
        '''
        Analyze existing places and search for new once
        '''
        try:
            if self.location:
                locations = Location.objects.filter(metro__slug=self.location)
            else:
               locations = Location.objects.all()
            for location in locations:
                self._analyze_existing_places(location)
                self._find_new_places(location)
            print "Facebook place analysis is complete!"
            self.logger.info("Facebook place analysis is complete!")
        except exceptions.OAuthError as e:
            import traceback
            print traceback.format_exc()
            self.elogger.error("Counter:%s in %s seconds" % (self.counter, elapsed))
            self.elogger.error(traceback.format_exc())
            self.logger.info("Counter:%s" % self.counter)
            self.logger.info("Received OAuth error! Exiting...")
            raise
        except Exception as e:
            import traceback
            print traceback.format_exc()
            self.elogger.error(traceback.format_exc())
            self.logger.info("Error occurred, see error log - exiting!")
            sys.exit(1)

class FBEventSearch(FBSearch):
    '''
        Facebook Event search class
    '''
    def __init__(self, **kw):
        # first set defaults which might get overwritten by super ie FBSearch.__init__()
        self.min_member_count = 300 # minimum member count to qualify for an event.
        # initialize logging
        self.elogger = FBSearch._initialize_error_logger(log='/var/log/fbevents/fbevents_error.log')
        self.wlogger = FBSearch._initialize_warning_logger(log='/var/log/fbevents/fbevents_warning.log')
        self.logger = FBSearch._initialize_info_logger(log='/var/log/fbevents/fbevents.log')
        self.logger.info("Initializing fbevents process...")
        super(FBEventSearch, self).__init__(**kw)

    def _initialize_keywords(self):
        self.CKEYWORDS = Keyword.objects.filter(category__name='club', section=KeywordSection.Body)
        self.CHEADKEYWORDS = Keyword.objects.filter(category__name='club', section=KeywordSection.Head)
        self.MKEYWORDS = Keyword.objects.filter(category__name='music', section=KeywordSection.Body)
        self.MHEADKEYWORDS = Keyword.objects.filter(category__name='music', section=KeywordSection.Head)

    def _assign_event_rscore(self, data, page):
        # Start everything from zero
        mscore = 0
        cscore = 0
        name = data['name'].lower()
        description = ' '.join([name, data['description'].lower()])
        length = len(description)
        # calculate score based on keywords, each instance gets X points
        for keyword in self.CKEYWORDS:
            cscore += (description.count(keyword.word) * keyword.points)
        for keyword in self.MKEYWORDS:
            mscore += (description.count(keyword.word) * keyword.points)

        # deduct points for events where keywords don't appear and have good number of characters
        # in description.
        if length > 500:
            if mscore == 0:
                mscore -=1
            if cscore == 0:
                cscore -= 1
        # descriptions larger than 500 characters might have too many keywords
        # reduce score based on how large character number is
        chunks = int(length/700)
        if int > 1 and chunks > 0:
            if mscore < -1 or mscore > 1:
                mscore = mscore / chunks
            if cscore < -1 or cscore > 1:
                cscore = cscore /chunks

        # events with birthdays get big minus, if not created by venue page.
        for keyword in self.CHEADKEYWORDS:
            if name.count(keyword.word):
                if keyword.word == 'birthday' or keyword.word == 'b-day' or keyword.word == 'turning' or keyword.word == 'bday':
                    if int(data['creator']) != int(data['venue']['id']) and int(data['all_members_count']) < 500:
                        cscore += keyword.points
                else:
                    cscore += keyword.points

        for keyword in self.MHEADKEYWORDS:
            if name.count(keyword.word):
                if keyword.word == 'birthday' or keyword.word == 'b-day' or keyword.word == 'turning' or keyword.word == 'bday':
                    if int(data['creator']) != int(data['venue']['id']) and int(data['all_members_count']) < 500:
                        mscore += keyword.points
                else:
                    mscore += keyword.points

        local_tz = page.location.metro.timezone
        # events that end between 01 and 06 get club boost
        if 'end_time' in data and data['end_time']:
            m = re.search('(T\d\d:\d\d:\d\d-\d\d\d\d)', data['end_time'])
            if m:
                t = parse_datetime(data['end_time'])
                local_time = t.astimezone(local_tz)
                if local_time.time() >= datetime.time(01,00) and local_time.time() <= datetime.time(06,00):
                    cscore += 1

        m = re.search('(T\d\d:\d\d:\d\d-\d\d\d\d)', data['start_time'])
        if m:
            t = parse_datetime(data['start_time'])
            local_time = t.astimezone(local_tz)
            if local_time.time() >= datetime.time(21,00) and local_time.time() <= datetime.time(23,00):
                cscore += 1
            if local_time.time() >= datetime.time(19,00) and local_time.time() < datetime.time(21,30):
                mscore += 1
         
        # Sample average rscore and add/deduct points based on historic data
        if Event.objects.filter(place=page).count() > 7:
            caverage = RScore.objects.filter(event__place=page, category__name = 'club').aggregate(Avg('score'))
            if caverage:
                if caverage['score__avg'] < 0:
                    cscore -= 2
                elif caverage['score__avg'] > 2 and length < 200:
                    cscore += 1
            maverage = RScore.objects.filter(event__place=page, category__name = 'music').aggregate(Avg('score'))
            if maverage:
                if maverage['score__avg'] < 0:
                    mscore -= 2
                elif maverage['score__avg'] > 2 and length < 200:
                    mscore += 1

        # add/deduct points based on category
        if page.category.filter(Q(name='Night Club')).exists():
            cscore += 1
        if page.category.filter(Q(name='Dance Club')).exists():
            cscore += 1
        if page.category.filter(Q(name='Concert Venue')).exists():
            mscore += 1
        if page.category.filter(Q(name='Performance Venue')).exists():
            mscore += 1
        if page.category.filter(Q(name='Jazz Club')).exists():
            mscore += 1
        if page.category.filter(Q(name='Night Club')|Q(name='Dance Club')).exists() \
            and not page.category.filter(Q(name='Concert Venue')|Q(name='Performance Venue')|Q(name='Jazz Club')).exists():
            mscore -= 1
        elif page.category.filter(Q(name='Concert Venue')|Q(name='Performance Venue')|Q(name='Jazz Club')).exists() \
            and not page.category.filter(Q(name='Night Club')|Q(name='Dance Club')).exists():
            cscore -= 1

        # minor score adjustment for definitive category
        if cscore == 2 and mscore > 6:
            cscore -= 1
        if mscore == 2 and cscore > 6:
            mscore -= 1

        # adjust scores so that one event won't skew averages
        if cscore < -8:
            cscore = -8
        if cscore > 8:
            cscore = 8
        if mscore < -8:
            mscore = -8
        if mscore > 8:
            mscore = 8

        return cscore, mscore

    def _process_new_events(self, page, events):
        for event in events:
            eventExists = True
            try:
                eventObj = Event.objects.get(fb_id=events[event]['eid'])
            except Event.DoesNotExist:
                eventExists = False
                eventObj = Event()

            if self.update or not eventExists or events[event]['update_time'] != eventObj.update_time:
                crscore, mrscore = self._assign_event_rscore(events[event], page)
                defaults = {
                            'update_time': events[event]['update_time'],
                            'name':events[event]['name'],
                            'description':events[event]['description'],
                            'data': events[event]}
                m = re.search('(\d{4}-\d{2}-\d{2})T', events[event]['start_time'])
                if m:
                    defaults['start_date'] = parse_date(m.group(1))
                    defaults['start_time'] = parse_datetime(events[event]['start_time'])
                else:
                    defaults['start_date'] = parse_date(events[event]['start_time'])
                if 'end_time' in events[event] and events[event]['end_time']:
                    m = re.search('(\d{4}-\d{2}-\d{2})T', events[event]['end_time'])
                    if m:
                        defaults['end_time'] = parse_datetime(events[event]['end_time'])
                if not eventExists:
                    print "Adding %d %d %s" % (events[event]['eid'], events[event]['all_members_count'], events[event]['start_time'])
                    self.logger.info("Adding %d %d %s" \
                      % (events[event]['eid'], events[event]['all_members_count'], events[event]['start_time']))
                else:
                    print "Updating %s old timestamp %s new timestamp %s" % (events[event]['eid'], eventObj.update_time, events[event]['update_time'])
                    self.logger.info("Updating %s old timestamp %s new timestamp %s" \
                          % (events[event]['eid'], eventObj.update_time, events[event]['update_time']))
                eventObj.data = events[event]
                if 'start_time' in defaults:
                    eventObj.start_time = defaults['start_time']
                if 'end_time' in defaults:
                    eventObj.end_time = defaults['end_time']
                eventObj.start_date = defaults['start_date']
                eventObj.update_time = events[event]['update_time']
                eventObj.name = events[event]['name']
                eventObj.place = page
                eventObj.description = events[event]['description']
                eventObj.fb_id=events[event]['eid']
                if events[event]['has_profile_pic'] == True:
                    # handling only if has_profile_pic is true, otherwise will be False by default
                    eventObj.has_profile_pic = True
                eventObj.save()
                if not eventObj.category_override:
                    eventObj.category.clear()
                    if mrscore >= 2:
                        eventObj.category.add(EventCategory.objects.get(name='music'))
                    if crscore >= 2:
                        eventObj.category.add(EventCategory.objects.get(name='club'))
                    eventObj.save()

                    cObj, created = RScore.objects.get_or_create(event=eventObj,
                                                                 category=EventCategory.objects.get(name='club'),
                                                                defaults={'score':crscore}
                    )
                    if not created:
                        cObj.score = crscore
                        cObj.save()
                    mObj, created = RScore.objects.get_or_create(event=eventObj,
                                                                 category=EventCategory.objects.get(name='music'),
                                                                defaults={'score':mrscore}
                    )
                    if not created:
                        mObj.score = mrscore
                        mObj.save()

        # now look at existing events
        existing = Event.objects.filter(place=page, start_date__gte=self.today)
        found_events = [events[e]['eid'] for e in events]
        for e in existing:
            if e.fb_id not in found_events:
                print "Deleting event %s because it is not in new list" % e.fb_id
                self.logger.info("Deleting event %s because it is not in new list" % e.fb_id)
                e.delete()


    def _process_location(self, location):
        self._initialize_keywords()
        equery = []
        if self.quarantine:
            equery.append(Q(status=PlaceStatus.Quarantine))
        if self.blacklist:
            equery.append(Q(status=PlaceStatus.Blacklist))
        q = Q(status=PlaceStatus.Active)
        q |= Q(status=PlaceStatus.Active_events)
        for item in equery:
            q |= item
        pages = Place.objects.filter(q, Q(location=location))
        plist = [str(page.fb_id) for page in pages]
        self.today = date.today()
        json = self.fbgraph('SELECT name, eid,creator, all_members_count, venue, has_profile_pic, description, start_time, end_time, \
                             update_time, hide_guest_list, pic_big, pic_cover, ticket_uri from event \
                             WHERE (eid IN (SELECT eid FROM event_member WHERE uid IN \
                             (SELECT page_id FROM place WHERE page_id IN (%s))) \
                              AND venue.id IN (%s) AND start_time >= "%s")' % (','.join(plist),
                                                                              ','.join(plist),
                                                                                self.today))
        self.counter += 1
        event_map = {}
        for event in json['data']:
            vid = event['venue']['id']
            id = event['eid']
            if vid not in event_map:
                event_map[vid] = {}
            event_map[vid][id] = event

        http_events = []
        for page in pages:
            if self.counter % 100 == 0:
                end = time.time()
                elapsed = end - self.proc_start_time
                print 'COUNTER>>>>>>>>>>>>>>>%s in %s seconds' % (self.counter, elapsed)
                print 'Memory usage: %s (kb)' % resource.getrusage(resource.RUSAGE_SELF).ru_maxrss
                self.logger.info('COUNTER>>>>>>>>>>>>>>>%s in %s seconds' % (self.counter, elapsed))
                self.logger.info('Memory usage: %s (kb)' % resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
            fb_search_criteria = page.get_fb_search_criteria()
            json_http = self.fbgraph('search?q=%s&type=event&fields=id,start_time,venue'
                                         % fb_search_criteria, 'get')
            self.counter += 1
            #pprint.pprint(json_http['data'])
            for event in json_http['data']:
                if 'id' not in event.get('venue', {}):
                    continue
                vid = int(event['venue']['id'])
                id = int(event['id'])
                if vid != page.fb_id:
                    continue
                if vid not in event_map or id not in event_map[vid]:
                    http_events.append(id)
                else:
                    continue
        if http_events:
            self.counter += 1
            json = self.fbgraph('SELECT name, eid, creator, all_members_count, venue, has_profile_pic, description, start_time, ticket_uri, \
                                 hide_guest_list, update_time, pic_big, pic_cover, end_time from event WHERE eid IN (%s) \
                                 AND start_time >= "%s"' % (','.join([ str(x) for x in http_events]), self.today))
            for event in json['data']:
                if event['all_members_count'] < self.min_member_count \
                    and event['hide_guest_list'] != True \
                    and not event['ticket_uri']:
                    continue
                vid = event['venue']['id']
                id = event['eid']
                if vid not in event_map:
                    event_map[vid] = {}
                event_map[vid][id] = event
            # now that we got our event bucket compare them to what we have in DB
        for page in pages:
            if page.fb_id in event_map:
                self._process_new_events(page, event_map[page.fb_id])

    def search_events(self):
        try:
            self.counter = 0
            self.proc_start_time = time.time()
            self.logger.info('Memory usage: %s (kb)' % resource.getrusage(resource.RUSAGE_SELF).ru_maxrss)
            while True:
                # To avoid populating the queryset cache, but to still iterate over all your results
                # for page in pages.iterator():
                # each iteration execute another query, rather than
                try:
                    if self.location:
                        locations = Location.objects.filter(metro__slug=self.location)
                    else:
                        locations = Location.objects.all()
                    for location in locations:
                        print "Processing %s" % location.metro.name
                        self.logger.info("Processing %s" % location.metro.name)
                        self._process_location(location)
                    if self.single_run:
                        end = time.time()
                        elapsed = end - self.proc_start_time
                        print "Counter:%s in %s seconds" % (self.counter, elapsed)
                        print "Facebook event search is complete!"
                        self.logger.info("Counter:%s in %s seconds" % (self.counter, elapsed))
                        self.logger.info("Facebook event search is complete!")
                        break
                except exceptions.OAuthError as e:
                    end = time.time()
                    elapsed = end - self.proc_start_time
                    import traceback
                    self.elogger.error("Counter:%s in %s seconds" % (self.counter, elapsed))
                    self.elogger.error(traceback.format_exc())
                    self.logger.info("Counter:%s" % self.counter)
                    self.logger.info("Received OAuth error %s! Pausing..." % e.code)
                    if e.code == 368:
                        time.sleep(3600)
                    else:
                        time.sleep(60)
                except Exception as e:
                    raise
        except Exception:
            end = time.time()
            elapsed = end - self.proc_start_time
            import traceback
            print "Counter:%s in %s seconds" % (self.counter, elapsed)
            print "Received an exception, check error log! Exiting..."
            print traceback.format_exc()
            self.elogger.error("Counter:%s in %s seconds" % (self.counter, elapsed))
            self.elogger.error(traceback.format_exc())
            self.logger.info("Counter:%s" % self.counter)
            self.logger.info("Received an exception, check error log! Exiting...")
            #import pdb
            #pdb.post_mortem()
            #An exception of type OAuthError occured. Arguments:
            #(u'[17] (#17) User request limit reached',)
            #if json is not None:
            #    pprint.pprint(json)
            #template = "An exception of type {0} occured. Arguments:\n{1!r}"
            #message = template.format(type(e).__name__, e.args)
            #self.elogger.error(message)
            #sleep(60)
            sys.exit(1)
        except exceptions.OAuthError:
            end = time.time()
            elapsed = end - self.proc_start_time
            import traceback
            print traceback.format_exc()
            self.elogger.error("Counter:%s in %s seconds" % (self.counter, elapsed))
            self.elogger.error(traceback.format_exc())
            self.logger.info("Counter:%s" % self.counter)
            self.logger.info("Received OAuth error! Exiting...")
            raise
        #pprint.pprint(event_map)
        #print self.total
        #print self.etotal
        #print self.counter
        #sys.exit()



