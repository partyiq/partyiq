from jsonfield import JSONField
from django.contrib.gis.db import models
import collections
from autoslug import AutoSlugField
from django_extensions.db import fields as ext_fields
from djchoices import DjangoChoices, ChoiceItem
from events.utils import convert_unicode_to_string
from timezone_field import TimeZoneField
from allauth.account.models import EmailAddress
from django.contrib.auth.models import User
from allauth.socialaccount.models import SocialAccount
import hashlib

class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile')

    def __unicode__(self):
        return "{}'s profile".format(self.user.username)

    class Meta:
        db_table = 'user_profile'

    def account_verified(self):
        if self.user.is_authenticated:
            result = EmailAddress.objects.filter(email=self.user.email)
            if len(result):
                return result[0].verified
        return False

    def profile_image_url(self):
        fb_uid = SocialAccount.objects.filter(user_id=self.user.id, provider='facebook')
        if len(fb_uid):
            return "http://graph.facebook.com/{}/picture?width=12&height=12".format(fb_uid[0].uid)
        return "http://www.gravatar.com/avatar/{}?s=25".format(hashlib.md5(self.user.email).hexdigest())

User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


class PlaceStatus(DjangoChoices):
    # Active state means that place is visible and active checks are performed continuously
    Active = ChoiceItem("A")
    # Active_events means that place is visible only when event is present
    # otherwise it's hidden, also events screened
    Active_events = ChoiceItem("E")
    # All newly imported places automatically end up in quarantine, which means we need to asses where
    # it belongs.  Place stays in quarantine until decision is made to either move it up or down to blacklist
    # events checks performed but not visible (only to admins)
    # Checks performed every 3 days
    Quarantine = ChoiceItem("Q")
    # If place is deemed as not worthy it moves from quarantine to blacklist, we still do event checks
    # but not as often.
    # Checks performed once every X days
    Blacklist = ChoiceItem("B")
    # Pages in banned status are deemed not to be related to night clubs, no checks are performed.  Have to
    # be absolutely sure that it's no worthy.  No checks performed.
    Banned = ChoiceItem("X")

class KeywordSection(DjangoChoices):
    Body = ChoiceItem("B")
    Head = ChoiceItem("H")

class Metro(models.Model):
    name = models.CharField(max_length=100, unique=True)
    slug = AutoSlugField(unique_with=['name'])
    timezone = TimeZoneField()

    def __unicode__(self):
        return self.name

class Location(models.Model):
    center = models.PointField(help_text="Represented as (longitude, latitude)", unique=True, db_index=True)
    distance = models.IntegerField(default=50000)
    metro = models.ForeignKey(Metro, null=True, blank=True, on_delete=models.CASCADE)
    objects = models.GeoManager()

    def __unicode__(self):
        metro = ''
        if self.metro:
            metro = self.metro
        return "%s %s,%s" % (metro, self.center.x, self.center.y)

class PlaceCategory(models.Model):
    fb_id = models.BigIntegerField(unique=True)
    name = models.CharField(db_index=True, max_length=20, unique=True, default='Night Club')

    def __unicode__(self):
        return self.name

class Place(models.Model):
    # Automatically create a unique id for this object
    uuid = ext_fields.UUIDField(auto=True)
    # id of facebook place
    fb_id = models.BigIntegerField(unique=True, default='7777')
    fb_search_criteria = models.CharField(max_length=100, blank=True, null=True, default=None)
    # name of facebook place
    name = models.CharField(max_length=100)
    # cached facebook data
    data = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict})
    slug = AutoSlugField(populate_from='name', unique_with='uuid', editable=True)
    added = models.DateTimeField(auto_now_add=True)
    last_status_change = models.DateTimeField(auto_now_add=True)
    point = models.PointField(help_text="Represented as (longitude, latitude)")
    status = models.CharField(db_index=True, max_length=1,
                            choices=PlaceStatus.choices,
                            default='Q')
    location = models.ForeignKey(Location, default=1, on_delete=models.CASCADE)
    category = models.ManyToManyField(PlaceCategory, blank=True, null=True, db_index=True)
    category_override = models.BooleanField(default=False)
    objects = models.GeoManager()

    def __unicode__(self):
        return "%s:%s" % (self.name, self.fb_id)

    def get_fb_search_criteria(self):
        """
            Method for retrieving fb_search_criteria field, converts name to
            ascii and strips special characters which impede fql search or
            returns fb_search_criteria field if defined
        :return:
        """
        if self.fb_search_criteria:
            return self.fb_search_criteria
        else:
            # strip unicode plus extra characters
            name = convert_unicode_to_string(self.name)
            name = name.translate(None, '|\/?><:;[]}{`~!@#$",.*%^&()-+=_')
            name = name.translate(None,"'")
            return name

class EventCategory(models.Model):
    name = models.CharField(db_index=True, max_length=20, unique=True, default='club')

    def __unicode__(self):
        return self.name

class Keyword(models.Model):
    word = models.CharField(max_length=50)
    category = models.ForeignKey(EventCategory, on_delete=models.CASCADE)
    section = models.CharField(max_length=1, choices=KeywordSection.choices, default='B')
    points = models.SmallIntegerField()



class Event(models.Model):
    fb_id = models.BigIntegerField(unique=True)
    update_time = models.PositiveIntegerField()
    # We might allow an ability to add events without tracking place in database
    # these might be one off events added by users or admins.
    # Current place attribute doesn't allow that
    place = models.ForeignKey(Place, on_delete=models.CASCADE, db_index=True)
    # cached data
    data = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict})
    start_time = models.DateTimeField(blank=True, null=True)
    end_time = models.DateTimeField(blank=True, null=True)
    start_date = models.DateField(db_index=True)
    name = models.TextField(db_index=True)
    description = models.TextField(null=True, blank=True)
    category = models.ManyToManyField(EventCategory, blank=True)
    category_override = models.BooleanField(default=False)
    has_profile_pic = models.BooleanField(default=False)
    objects = models.GeoManager()

    def __unicode__(self):
        return self.name

class RScore(models.Model):
    score = models.SmallIntegerField()
    category = models.ForeignKey(EventCategory, on_delete=models.PROTECT)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
