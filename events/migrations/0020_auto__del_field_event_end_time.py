# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Event.end_time'
        db.delete_column(u'events_event', 'end_time')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'Event.end_time'
        raise RuntimeError("Cannot reverse this migration. 'Event.end_time' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Event.end_time'
        db.add_column(u'events_event', 'end_time',
                      self.gf('django.db.models.fields.DateTimeField')(db_index=True),
                      keep_default=False)


    models = {
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'data': ('jsonfield.fields.JSONField', [], {}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Place']"}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1', 'db_index': 'True'})
        },
        u'events.location': {
            'Meta': {'object_name': 'Location'},
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {'unique': 'True', 'db_index': 'True'}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '50000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metro': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Metro']", 'null': 'True', 'blank': 'True'})
        },
        u'events.metro': {
            'Meta': {'object_name': 'Metro'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "['name']", 'max_length': '50', 'populate_from': 'None'})
        },
        u'events.place': {
            'Meta': {'object_name': 'Place'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': "'7777'", 'unique': 'True'}),
            'fb_search_criteria': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['events.Location']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'same_as': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['events.Place']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "('uuid',)", 'max_length': '50', 'populate_from': "'name'"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Q'", 'max_length': '1', 'db_index': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36', 'blank': 'True'})
        }
    }

    complete_apps = ['events']