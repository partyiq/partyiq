# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Place', fields ['name']
        db.delete_unique(u'events_place', ['name'])

        # Adding field 'Place.uuid'
        db.add_column(u'events_place', 'uuid',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=36, blank=True),
                      keep_default=False)

        # Adding field 'Place.slug'
        db.add_column(u'events_place', 'slug',
                      self.gf('autoslug.fields.AutoSlugField')(default=datetime.datetime(2014, 1, 29, 0, 0), unique_with=['name', 'uuid'], max_length=50, populate_from=None),
                      keep_default=False)


        # Changing field 'Place.fb_id'
        db.alter_column(u'events_place', 'fb_id', self.gf('django.db.models.fields.BigIntegerField')(unique=True, null=True))

        # Changing field 'Event.fb_id'
        db.alter_column(u'events_event', 'fb_id', self.gf('django.db.models.fields.BigIntegerField')(unique=True, null=True))

    def backwards(self, orm):
        # Deleting field 'Place.uuid'
        db.delete_column(u'events_place', 'uuid')

        # Deleting field 'Place.slug'
        db.delete_column(u'events_place', 'slug')

        # Adding unique constraint on 'Place', fields ['name']
        db.create_unique(u'events_place', ['name'])


        # Changing field 'Place.fb_id'
        db.alter_column(u'events_place', 'fb_id', self.gf('django.db.models.fields.BigIntegerField')(default=datetime.datetime(2014, 1, 29, 0, 0), unique=True))

        # Changing field 'Event.fb_id'
        db.alter_column(u'events_event', 'fb_id', self.gf('django.db.models.fields.BigIntegerField')(default=datetime.datetime(2014, 1, 29, 0, 0), unique=True))

    models = {
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'data': ('jsonfield.fields.JSONField', [], {}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': 'None', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Place']"}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '2'})
        },
        u'events.place': {
            'Meta': {'object_name': 'Place'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': 'None', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "['name', 'uuid']", 'max_length': '50', 'populate_from': 'None'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36', 'blank': 'True'})
        }
    }

    complete_apps = ['events']