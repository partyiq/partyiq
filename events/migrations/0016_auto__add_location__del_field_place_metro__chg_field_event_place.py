# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Location'
        db.create_table(u'events_location', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('center', self.gf('django.contrib.gis.db.models.fields.PointField')()),
            ('distance', self.gf('django.db.models.fields.IntegerField')(default=50000)),
            ('metro', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['events.Metro'], null=True, blank=True)),
        ))
        db.send_create_signal(u'events', ['Location'])

        # Deleting field 'Place.metro'
        db.delete_column(u'events_place', 'metro_id')


        # Changing field 'Event.place'
        db.alter_column(u'events_event', 'place_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['events.Place']))

    def backwards(self, orm):
        # Deleting model 'Location'
        db.delete_table(u'events_location')

        # Adding field 'Place.metro'
        db.add_column(u'events_place', 'metro',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['events.Metro']),
                      keep_default=False)


        # Changing field 'Event.place'
        db.alter_column(u'events_event', 'place_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['events.Place'], on_delete=models.PROTECT))

    models = {
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'data': ('jsonfield.fields.JSONField', [], {}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': 'None', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Place']"}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1', 'db_index': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'events.location': {
            'Meta': {'object_name': 'Location'},
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '50000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metro': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Metro']", 'null': 'True', 'blank': 'True'})
        },
        u'events.metro': {
            'Meta': {'object_name': 'Metro'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "['name']", 'max_length': '50', 'populate_from': 'None'})
        },
        u'events.place': {
            'Meta': {'object_name': 'Place'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': "'7777'", 'unique': 'True'}),
            'fb_search_criteria': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'same_as': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['events.Place']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "('uuid',)", 'max_length': '50', 'populate_from': "'name'"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Q'", 'max_length': '1', 'db_index': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36', 'blank': 'True'})
        }
    }

    complete_apps = ['events']