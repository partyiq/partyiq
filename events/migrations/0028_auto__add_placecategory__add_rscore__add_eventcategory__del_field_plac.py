# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PlaceCategory'
        db.create_table(u'events_placecategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fb_id', self.gf('django.db.models.fields.BigIntegerField')(unique=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='club', unique=True, max_length=20, db_index=True)),
        ))
        db.send_create_signal(u'events', ['PlaceCategory'])

        # Adding model 'RScore'
        db.create_table(u'events_rscore', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('score', self.gf('django.db.models.fields.SmallIntegerField')()),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['events.EventCategory'], on_delete=models.PROTECT)),
            ('event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['events.Event'])),
        ))
        db.send_create_signal(u'events', ['RScore'])

        # Adding model 'EventCategory'
        db.create_table(u'events_eventcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='club', unique=True, max_length=20, db_index=True)),
        ))
        db.send_create_signal(u'events', ['EventCategory'])

        # Deleting field 'Place.same_as'
        db.delete_column(u'events_place', 'same_as_id')

        # Adding field 'Place.category_override'
        db.add_column(u'events_place', 'category_override',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding M2M table for field category on 'Place'
        m2m_table_name = db.shorten_name(u'events_place_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('place', models.ForeignKey(orm[u'events.place'], null=False)),
            ('placecategory', models.ForeignKey(orm[u'events.placecategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['place_id', 'placecategory_id'])

        # Deleting field 'Event.status'
        db.delete_column(u'events_event', 'status')

        # Deleting field 'Event.rscore'
        db.delete_column(u'events_event', 'rscore')

        # Adding M2M table for field category on 'Event'
        m2m_table_name = db.shorten_name(u'events_event_category')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('event', models.ForeignKey(orm[u'events.event'], null=False)),
            ('eventcategory', models.ForeignKey(orm[u'events.eventcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['event_id', 'eventcategory_id'])


    def backwards(self, orm):
        # Deleting model 'PlaceCategory'
        db.delete_table(u'events_placecategory')

        # Deleting model 'RScore'
        db.delete_table(u'events_rscore')

        # Deleting model 'EventCategory'
        db.delete_table(u'events_eventcategory')

        # Adding field 'Place.same_as'
        db.add_column(u'events_place', 'same_as',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['events.Place'], null=True, on_delete=models.SET_NULL, blank=True),
                      keep_default=False)

        # Deleting field 'Place.category_override'
        db.delete_column(u'events_place', 'category_override')

        # Removing M2M table for field category on 'Place'
        db.delete_table(db.shorten_name(u'events_place_category'))

        # Adding field 'Event.status'
        db.add_column(u'events_event', 'status',
                      self.gf('django.db.models.fields.CharField')(default='A', max_length=1, db_index=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'Event.rscore'
        raise RuntimeError("Cannot reverse this migration. 'Event.rscore' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'Event.rscore'
        db.add_column(u'events_event', 'rscore',
                      self.gf('django.db.models.fields.SmallIntegerField')(),
                      keep_default=False)

        # Removing M2M table for field category on 'Event'
        db.delete_table(db.shorten_name(u'events_event_category'))


    models = {
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['events.EventCategory']", 'symmetrical': 'False', 'blank': 'True'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Place']"}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'update_time': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'events.eventcategory': {
            'Meta': {'object_name': 'EventCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'club'", 'unique': 'True', 'max_length': '20', 'db_index': 'True'})
        },
        u'events.location': {
            'Meta': {'object_name': 'Location'},
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {'unique': 'True', 'db_index': 'True'}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '50000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metro': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Metro']", 'null': 'True', 'blank': 'True'})
        },
        u'events.metro': {
            'Meta': {'object_name': 'Metro'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "['name']", 'max_length': '50', 'populate_from': 'None'})
        },
        u'events.place': {
            'Meta': {'object_name': 'Place'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['events.PlaceCategory']", 'symmetrical': 'False'}),
            'category_override': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': "'7777'", 'unique': 'True'}),
            'fb_search_criteria': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['events.Location']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "('uuid',)", 'max_length': '50', 'populate_from': "'name'"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Q'", 'max_length': '1', 'db_index': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36', 'blank': 'True'})
        },
        u'events.placecategory': {
            'Meta': {'object_name': 'PlaceCategory'},
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'club'", 'unique': 'True', 'max_length': '20', 'db_index': 'True'})
        },
        u'events.rscore': {
            'Meta': {'object_name': 'RScore'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.EventCategory']", 'on_delete': 'models.PROTECT'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'score': ('django.db.models.fields.SmallIntegerField', [], {})
        }
    }

    complete_apps = ['events']