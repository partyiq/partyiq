# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Event.category_override'
        db.add_column(u'events_event', 'category_override',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Event.category_override'
        db.delete_column(u'events_event', 'category_override')


    models = {
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['events.EventCategory']", 'symmetrical': 'False', 'blank': 'True'}),
            'category_override': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.TextField', [], {'db_index': 'True'}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Place']"}),
            'start_date': ('django.db.models.fields.DateField', [], {'db_index': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'update_time': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'events.eventcategory': {
            'Meta': {'object_name': 'EventCategory'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'club'", 'unique': 'True', 'max_length': '20', 'db_index': 'True'})
        },
        u'events.location': {
            'Meta': {'object_name': 'Location'},
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {'unique': 'True', 'db_index': 'True'}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '50000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metro': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Metro']", 'null': 'True', 'blank': 'True'})
        },
        u'events.metro': {
            'Meta': {'object_name': 'Metro'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "['name']", 'max_length': '50', 'populate_from': 'None'})
        },
        u'events.place': {
            'Meta': {'object_name': 'Place'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'category': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['events.PlaceCategory']", 'null': 'True', 'db_index': 'True', 'blank': 'True'}),
            'category_override': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': "'7777'", 'unique': 'True'}),
            'fb_search_criteria': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.related.ForeignKey', [], {'default': '1', 'to': u"orm['events.Location']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "('uuid',)", 'max_length': '50', 'populate_from': "'name'"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Q'", 'max_length': '1', 'db_index': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36', 'blank': 'True'})
        },
        u'events.placecategory': {
            'Meta': {'object_name': 'PlaceCategory'},
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'unique': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "'Night Club'", 'unique': 'True', 'max_length': '20', 'db_index': 'True'})
        },
        u'events.rscore': {
            'Meta': {'object_name': 'RScore'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.EventCategory']", 'on_delete': 'models.PROTECT'}),
            'event': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Event']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'score': ('django.db.models.fields.SmallIntegerField', [], {})
        }
    }

    complete_apps = ['events']