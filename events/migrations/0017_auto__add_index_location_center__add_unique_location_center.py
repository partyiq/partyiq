# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding index on 'Location', fields ['center']
        db.create_index(u'events_location', ['center'])

        # Adding unique constraint on 'Location', fields ['center']
        db.create_unique(u'events_location', ['center'])


    def backwards(self, orm):
        # Removing unique constraint on 'Location', fields ['center']
        db.delete_unique(u'events_location', ['center'])

        # Removing index on 'Location', fields ['center']
        db.delete_index(u'events_location', ['center'])


    models = {
        u'events.event': {
            'Meta': {'object_name': 'Event'},
            'data': ('jsonfield.fields.JSONField', [], {}),
            'end_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': 'None', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_update': ('django.db.models.fields.DateTimeField', [], {}),
            'place': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Place']"}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'A'", 'max_length': '1', 'db_index': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        },
        u'events.location': {
            'Meta': {'object_name': 'Location'},
            'center': ('django.contrib.gis.db.models.fields.PointField', [], {'unique': 'True', 'db_index': 'True'}),
            'distance': ('django.db.models.fields.IntegerField', [], {'default': '50000'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'metro': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['events.Metro']", 'null': 'True', 'blank': 'True'})
        },
        u'events.metro': {
            'Meta': {'object_name': 'Metro'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "['name']", 'max_length': '50', 'populate_from': 'None'})
        },
        u'events.place': {
            'Meta': {'object_name': 'Place'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'data': ('jsonfield.fields.JSONField', [], {}),
            'fb_id': ('django.db.models.fields.BigIntegerField', [], {'default': "'7777'", 'unique': 'True'}),
            'fb_search_criteria': ('django.db.models.fields.CharField', [], {'default': 'None', 'max_length': '100', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'point': ('django.contrib.gis.db.models.fields.PointField', [], {}),
            'same_as': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['events.Place']", 'null': 'True', 'on_delete': 'models.SET_NULL', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': "('uuid',)", 'max_length': '50', 'populate_from': "'name'"}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Q'", 'max_length': '1', 'db_index': 'True'}),
            'uuid': ('django.db.models.fields.CharField', [], {'max_length': '36', 'blank': 'True'})
        }
    }

    complete_apps = ['events']