from django import forms
import datetime

class FrontLandingForm(forms.Form):
    date = forms.DateField(required=True, input_formats=('%B %d, %Y',), widget=forms.DateInput(format = '%B %d, %Y'), initial=datetime.date.today())
    type = forms.CharField(initial='all', widget=forms.widgets.HiddenInput())

    def get_cleaned_or_initial(self, fieldname):
        if hasattr(self, 'cleaned_data'):
            return self.cleaned_data.get(fieldname)
        else:
            return self[fieldname].field.initial
