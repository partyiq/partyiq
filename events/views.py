from django.shortcuts import render_to_response
from events.forms import FrontLandingForm
from events.models import Event, Place, Metro
from django.contrib.gis.db.models import Count
from django.template import RequestContext
from django.db.models import Q
from events.facebook import FBSearch
from facepy import exceptions
from allauth.socialaccount.models import SocialToken, SocialAccount
import pprint
from django.contrib.auth import logout, login
import datetime

def front_landing(request, metro='san_francisco'):
    # get places
    data = {
        'places' : Place.objects.filter(Q(event__category__name='club'),Q(location__metro__slug='san_francisco')).annotate(Count('event')).order_by('-event__count').distinct()[:8],
        'metros' : Metro.objects.all(),
        'metro' : Metro.objects.get(slug=metro)
    }
    if request.method == 'POST': # If the form has been submitted...
        form = FrontLandingForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            fdate = form.cleaned_data['date']
            data['type'] = form.cleaned_data['type']
            data['date'] = fdate
    else:
        form = FrontLandingForm() # An unbound form
        data['date'] = form.get_cleaned_or_initial('date')
        data['type'] = form.get_cleaned_or_initial('type')
    data['form'] = form

    events = Event.objects.filter(place__location__metro__slug=metro, has_profile_pic=True)
    equery = []
    if data['type'] == 'all' or data['type'] == 'music':
        equery.append(Q(category__name='music'))
    if data['type'] == 'all' or data['type'] == 'clubs':
        equery.append(Q(category__name='club'))
    q = equery.pop()
    for item in equery:
        q |= item
    events = events.filter(q).distinct()
    events = events.filter(start_date=data['date'])
    data['events'] = events[:9]
    if request.user.is_authenticated() and valid_token(request):
        eids = [str(event.fb_id) for event in events[:9]]
        fbsearch = FBSearch(graph_speed = 0, token = SocialToken.objects.get(account__user=request.user, account__provider='facebook') )
        jsond = fbsearch.fbgraph('SELECT name, eid, all_members_count, venue, description, start_time, end_time, \
                     update_time, hide_guest_list, pic_big, pic_cover, ticket_uri from event \
                     WHERE eid in (%s)' % ','.join(eids))
        for item in jsond['data']:
            e = events.get(fb_id=str(item['eid']))
            item['data'] = e.data
            item['place'] = e.place
            item['start_time'] = e.start_time
        data['realtime_events'] = jsond['data']
        # get friends data
        user = SocialAccount.objects.get(user=request.user)
        offset = 0
        my_friends = fbsearch.fbgraph("me?fields=friends", "get")
        counter = len(my_friends['friends']['data'])
        batch_request = []
        while counter > 0:
            pfriends = 'SELECT uid2 FROM friend WHERE uid1=%s LIMIT 20 OFFSET %s' % (user.uid, str(offset))
            the_query = 'SELECT uid, eid from event_member where uid IN (%s) AND eid in (%s)' % (pfriends, ','.join(eids))
            batch_item = {"method":"GET","relative_url":"fql?q=" + the_query}
            batch_request.append(batch_item)
            offset = offset + 20
            counter = counter - 20
        jsond = fbsearch.fbgraph(batch_request, 'batch')
        for i in jsond:
            for item in i['data']:
                for event in data['realtime_events']:
                    if str(item['eid']) == str(event['eid']):
                        if 'friends' not in event:
                            event['friends'] = []
                        event['friends'].append(item)
        elist = sorted(data['realtime_events'], key=lambda k: len(k.get("friends", [])), reverse=True)
        # if number of events is less than 9 then we need to fill up
        # all the table cells.  In the future we can throw ads
        # but for now I am just cloning whatever we have
        # if there aren't any events then tough shit!
        if len(elist) < 9 and len(elist) != 0:
            x = 0
            while True:
                try:
                    elist[x]
                except IndexError:
                    x = 0
                elist.append(elist[x])
                if len(elist) == 9:
                    break
                x = x + 1
        data['realtime_events'] = elist
    else:
        # TODO write a function to do this
        data['events'] = list(data['events'])
        if len(data['events']) < 9 and len(data['events']) != 0:
            x = 0
            while True:
                try:
                    data['events'][x]
                except IndexError:
                    x = 0
                data['events'].append(data['events'][x])
                if len(data['events']) == 9:
                    break
                x = x + 1
    return render_to_response('search_result.html', data , context_instance=RequestContext(request))

def event_search(request, metro, type='all', date=datetime.date.today()):
    data = {
        'metros' : Metro.objects.all(),
        'metro' : Metro.objects.get(slug=metro),
        'guide' : 'events',
        'date' : date,
        'type' : type,
        'dateObj': datetime.datetime.strptime(str(date), "%Y-%m-%d").date()
    }
    events = Event.objects.filter(place__location__metro__slug=metro)
    equery = []
    if data['type'] == 'all' or data['type'] == 'music':
        equery.append(Q(category__name='music'))
    if data['type'] == 'all' or data['type'] == 'clubs':
        equery.append(Q(category__name='club'))
    q = equery.pop()
    for item in equery:
        q |= item
    events = events.filter(q).distinct()
    events = events.filter(start_date=data['date'])
    data['events'] = events
    if request.user.is_authenticated() and valid_token(request):
        eids = [str(event.fb_id) for event in events]
        fbsearch = FBSearch(graph_speed = 0, token = SocialToken.objects.get(account__user=request.user, account__provider='facebook') )
        jsond = fbsearch.fbgraph('SELECT name, eid, all_members_count, venue, description, start_time, end_time, \
                     update_time, hide_guest_list, pic_big, pic_cover, ticket_uri from event \
                     WHERE eid in (%s)' % ','.join(eids))
        for item in jsond['data']:
            e = events.get(fb_id=str(item['eid']))
            item['data'] = e.data
            item['place'] = e.place
            item['start_time'] = e.start_time
        data['realtime_events'] = jsond['data']
        # get friends data
        user = SocialAccount.objects.get(user=request.user)
        offset = 0
        my_friends = fbsearch.fbgraph("me?fields=friends", "get")
        counter = len(my_friends['friends']['data'])
        batch_request = []
        while counter > 0:
            pfriends = 'SELECT uid2 FROM friend WHERE uid1=%s LIMIT 20 OFFSET %s' % (user.uid, str(offset))
            the_query = 'SELECT uid, eid from event_member where uid IN (%s) AND eid in (%s)' % (pfriends, ','.join(eids))
            batch_item = {"method":"GET","relative_url":"fql?q=" + the_query}
            batch_request.append(batch_item)
            offset = offset + 20
            counter = counter - 20
        jsond = fbsearch.fbgraph(batch_request, 'batch')
        for i in jsond:
            for item in i['data']:
                for event in data['realtime_events']:
                    if str(item['eid']) == str(event['eid']):
                        if 'friends' not in event:
                            event['friends'] = []
                        event['friends'].append(item)
        elist = sorted(data['realtime_events'], key=lambda k: len(k.get("friends", [])), reverse=True)
        data['realtime_events'] = elist
    return render_to_response('event_search.html', data , context_instance=RequestContext(request))

def valid_token(request):
    try:
        # TODO improve this, this will throw an exception if non facebook user logs in (ie isarkiso)
        fbsearch = FBSearch(graph_speed = 0, token = SocialToken.objects.get(account__user=request.user, account__provider='facebook') )
        fbsearch.fbgraph('me', 'get')
    except exceptions.OAuthError:
        # this means FB connection has failed
        logout(request)
        # TODO force login?
        return False
    except Exception:
        return False
    return True
