from django.contrib.gis import admin
from . import models

class PlaceAdmin(admin.GeoModelAdmin):
    search_fields = ['name', 'fb_search_criteria', 'fb_id',  'status',  'location__metro__name', 'slug']
    list_display = ['fb_id', 'name', 'fb_search_criteria', 'slug', 'status', 'added',  'point', 'location', 'last_status_change' ]

admin.site.register(models.Place, PlaceAdmin)

class PlaceCategoryAdmin(admin.GeoModelAdmin):
    search_fields = ['fb_id', 'name']
    list_display = ['fb_id', 'name']

admin.site.register(models.PlaceCategory, PlaceCategoryAdmin)

class MetroAdmin(admin.GeoModelAdmin):
    search_fields = ['name']
    list_display = [ 'name', 'timezone' ]

admin.site.register(models.Metro, MetroAdmin)

class LocationAdmin(admin.GeoModelAdmin):
    search_fields = ['id', 'location__metro__name']
    list_display = [ 'id', 'center', ]

admin.site.register(models.Location, LocationAdmin)

class EventAdmin(admin.GeoModelAdmin):
    search_fields = ['fb_id', 'name', 'description', 'place__name', 'place__id', 'category__name', 'place__location__metro__name']
    list_display = ['fb_id', 'place', 'start_date']

admin.site.register(models.Event, EventAdmin)

class EventCategoryAdmin(admin.GeoModelAdmin):
    search_fields = ['name']
    list_display = ['name']

admin.site.register(models.EventCategory, EventCategoryAdmin)

class RScoreAdmin(admin.GeoModelAdmin):
    search_fields = ['score', 'event__name', 'event__place__name']
    list_display = ['score', 'category', 'event']

admin.site.register(models.RScore, RScoreAdmin)

class KeywordAdmin(admin.GeoModelAdmin):
    search_fields = ['word', 'category__name']
    list_display = ['word', 'category', 'section', 'points',]

admin.site.register(models.Keyword, KeywordAdmin)
