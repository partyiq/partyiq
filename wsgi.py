import os
import sys
import site

site.addsitedir("/var/www/partyiq.com/lib/python2.7/site-packages/")

from django.core.handlers.wsgi import WSGIHandler
sys.path.append('/var/www/partyiq.com/partyiq/partyrumbler')
sys.path.append('/var/www/partyiq.com/partyiq/')

os.environ['DJANGO_SETTINGS_MODULE'] = 'partyrumbler.settings'

# Activate your virtual env
activate_env=os.path.expanduser("/var/www/partyiq.com/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
